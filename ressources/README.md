# Ressources
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section liste des ressources sur le SNDS.

## Documents partagés en téléchargement
- [KWIKLY - Katalogue SNIIRAM SNDS](kwikly.md) [Cnam - MPL-2.0] permet de connaitre la présence des variables dans l’historique des produits depuis 2006.
- Guides pédagogiques du SNDS [Cnan - MPL-2.0] pour les [accès permanents](../files/Cnam/guides_pedagogiques/2019-07_Cnam_Guide_pedagogique_SNDS_acces_permanents_MPL-2.0.docx) et pour les [accès sur projet](../files/Cnam/guides_pedagogiques/2019-07_Cnam_Guide_pedagogique_SNDS_acces_projet_MPL-2.0.docx).  
- [Dictionnaire DCIR et PMSI/MCO](../files/GIS_EPI-PHARE/2019-04_GIS_EPI-PHARE_DICO_DCIR_4_Vue_MPL-2.0.xlsx) [GIS EPI-PHARE - MPL-2.0] utilisé en interne pour reconstruire des vues SAS. Contient notamment les années de disponibilité des tables et variables.
- [DCIR-Formats.zip](../files/GIS_EPI-PHARE/2019-04_GIS_EPI-PHARE_DCIR-Formats_MPL-2.0.zip) [GIS EPI-PHARE - MPL-2.0]  
Archive comprenant
  - `PI_DCIR_Formats.docx`, un manuel sur les vues et formats DCIR
  - 33  fichiers xlsx retraçant les évolutions mensuelles de certaines tables de valeurs
- [FAQ DCIR 09 2019](../files/Cnam/2019-07-CNAM-FAQ_DCIR_MLP-2.0.xlsx) et [FAQ EGB 09 2019](../files/Cnam/2019-06-CNAM-FAQ_EGB_MLP-2.0.xlsx) [CNAM - MPL-2.0] résument les questions fréquemment posées sur DCIR et l'EGB.


## Autres ressources disponibles en ligne

- Le nouveau [forum d'entraide](https://entraide.health-data-hub.fr) de la communauté des utilisateurs du SNDS.

- Le site [snds.gouv](https://www.snds.gouv.fr/SNDS/Accueil) 
pour des informations générales.

- Le [Wiki SNIIRAM](http://open-data-assurance-maladie.ameli.fr/wiki-sniiram/index.php/Accueil_-_Dictionnaire_de_donn%C3%A9es_SNIIRAM) 
et notamment sa [FAQ](http://open-data-assurance-maladie.ameli.fr/wiki-sniiram/index.php/Questions-R%C3%A9ponses),
édité par la Cnam.

- Le [site de l'ATIH](https://www.atih.sante.fr)
pour des détails sur le PMSI
    - partie [information médicale](https://www.atih.sante.fr/domaines-d-activites/information-medicale) du site
    - plateforme de restitution des données des établissements de santé [scansanté](https://www.scansante.fr) 

- Le [site du CépiDc](https://cepidc.inserm.fr/causes-medicales-de-deces/la-base-des-causes-medicales-de-deces)
pour des détails sur la base médicale des causes de décès.

- Un [dictionnaire interactif](https://drees.shinyapps.io/dico-snds/) du SNDS, produit par la DREES.

- Un [schema formalisé du SNDS](https://gitlab.com/healthdatahub/schema-snds), 
qui alimente le dictionnaire interactif, et la partie **Tables** de cette documentation.

- Le site open data du gouvernement : [data.gouv](https://www.data.gouv.fr) et la [partie dédiée à la santé](https://www.data.gouv.fr/fr/topics/sante-et-social/) avec notamment sur le profil de [l'assurance maladie](https://www.data.gouv.fr/fr/organizations/caisse-nationale-de-l-assurance-maladie-des-travailleurs-salaries/) qui répértorie tous les jeux de données open data de la Cnam.  


## Autres
- [Présentations et vidéos](meetup.md) des Meetup SNDS.
- [Programmes](programmes.md) dont le code est public.
- [Bibliographie](bibliographie.md)
  
