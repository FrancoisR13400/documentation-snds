# ER_CAM_F

Table des données de codage de la Classification Commune des Actes Médicaux


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|CAM_ACT_COD|chaîne de caractères|Code activite|||
|CAM_ACT_PRU|nombre réel|Prix unitaire CCAM de l'acte médical|||
|CAM_ASS_COD|chaîne de caractères|Code association|||
|CAM_CAB_IND|chaîne de caractères|Top supplément de charge en cabinet|||
|CAM_DOC_EXT|chaîne de caractères|Extension documentaire|||
|CAM_MOD_COD|chaîne de caractères|Codes modificateurs|||
|CAM_ORD_NUM|nombre réel|Numéro d'ordre de la prestation affinée CCAM|||
|CAM_PRS_IDE|chaîne de caractères|Code CCAM de l'acte médical|||
|CAM_QUA_DEN|chaîne de caractères|Localisation dentaire|||
|CAM_REM_BSE|nombre réel|Base de remboursement de la CCAM|||
|CAM_REM_COD|chaîne de caractères|Code remboursement exceptionnel|||
|CAM_TRT_PHA|nombre réel|Phase de traitement|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|DCT_ORD_NUM|nombre réel|N° ordre décompte dans caisse                      1|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
